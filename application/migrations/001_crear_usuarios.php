<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Crear_usuarios extends CI_Migration
{

    public function up()
    {
        //creamos la estructura de una tabla con un
        //id autoincremental, un campo varchar para el username
        //y otro para el passwords también varchar

        /*CREAR TABLA USUARIO*/
        $this->dbforge->add_field(
            array(
                "id" => array(
                    "type" => "INT",
                    "constraint" => 10,/*tamaño*/
                    "unsigned" => TRUE,
                    "auto_increment" => TRUE
                ),
                "email" => array(
                    "type" => "VARCHAR",
                    "constraint" => 100
                ),
                "password" => array(
                    "type" => "VARCHAR",
                    "constraint" => 50
                ),
                "activo" => array(
                    "type" => "BOOLEAN",
                    "default" => 1
                ),
                "rol" => array(
                    "type" => "INT",
                    "default" => 2
                )
            )
        );
        $this->dbforge->add_key('id', TRUE);//establecemos id como primary_key
        $this->dbforge->create_table('usuarios');//creamos la tabla users

        /*CREAR USUARIO ADMIN*/
        $datos_usuario = array(
            "email" => "desarrollo",
            "password" => md5("desarrollo"),
            "activo" => 0,
            "rol" => 1
        );
        //ingresamos el registro en la base de datos
        $this->db->insert("usuarios", $datos_usuario);
    }

    public function down()
    {
        $this->db->where("email", "desarrollo");
        $this->db->delete("usuarios");
        //eliminamos la tabla users
        $this->dbforge->drop_table('usuarios');

    }
}

/* End of file 001_crear_usuarios.php */
/* Location: ./application/migrations/001_crear_usuarios.php */