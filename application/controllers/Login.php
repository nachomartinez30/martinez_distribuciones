<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User: ignacio
 * Date: 12/09/2018
 * Time: 10:39 AM
 */
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index()
    {
        $this->load->view('login/login_vw');
    }

    public function checkin()
    {
        $correo = trim($_POST['correo']);
        $password = trim($_POST['password']);

        $resultados = $this->login_model->login($correo, $password);
        if ($resultados === false) {
//            var_dump($resultados->result());
//            header('Location: ' . base_url().'login');
        } else {
            foreach ($resultados->result() as $sesion) {
//                var_dump($_SESSION);
                $_SESSION['rol'] = $sesion->rol;
                $_SESSION['correo'] = $sesion->email;
            }
//            var_dump($_SESSION['rol']);
//            var_dump($_SESSION['correo']);
            header('Location: ' . base_url() );
        }
    }

    public function logout()
    {

        session_destroy();
        header('Location: ' . base_url());
    }
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */