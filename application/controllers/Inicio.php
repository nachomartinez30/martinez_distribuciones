<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        if($this->existSesion()){
            $this->load->view("inicio/sayHi");
        }else{
            header('Location:'.base_url().'login');
        }
	}



    public function existSesion()
    {
        if($_SESSION['rol'] != null && $_SESSION['correo'] != null){
            return true;
        }else{
            return false;
        }
    }

}

/* End of file Inicio.php */
/* Location: ./application/controllers/Inicio.php */