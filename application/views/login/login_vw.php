<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>M.D.</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <link href="favicon.ico" rel="shortcut icon">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
</head>

<body class="container-fluid">
<header>
    <div class="header_main row align-items-center justify-content-end">
        <div class="col-md-12 col-xs-12">
            <!--    <div class="col-md-2"><img class="img-fluid float-left" src="--><?php //echo URL_BASE.'/imagenes/logo.png'?><!--"></div>-->
            <h1 class="no-margin">Martinez Distribuciones</h1>
        </div>
    </div>
</header>
<main>
    <div class="py-5 h-50 my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <br>
                    <br>
                    <form action="login/login" method="post">
                        <div class="form-group">
                            <label for="txtCorreo">Correo electrónico</label>
                            <input id="txtCorreo" name="correo" type="text" class="form-control" placeholder="Correo">
                            <!--<small class="form-text text-muted">Utilize su correo institucional</small>-->
                        </div>
                        <div class="form-group">
                            <label for="txtPassword">Contraseña</label>
                            <input id="txtPassword" name="password" type="password" class="form-control" placeholder="Contraseña">
                        </div>
                        <button type="submit" class="btn btn-primary">Entrar</button>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
</main>
</body>
</html>