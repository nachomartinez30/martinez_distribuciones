$(document).ready(function () {
    $('#tblRecords').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "",
            "infoFiltered": "de _TOTAL_ registros totales",
            "search": "",
            "processing": "Procesando...",
            "searchPlaceholder": "Buscar:",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        },
    });
    $('input').addClass("form-control");
    $('select').addClass("form-line");
})
